from calculator import Calculator

while True:
    print("#### CALCULATOR ####")
    num1 = int(input("Enter first number: "))
    num2 = int(input("Enter second number: "))
 
    print("Select operation.")
    print("1. Add")
    print("2. Subtract")
    print("3. Multiply")
    print("4. Divide")
 
    choice = input("Enter choice(1/2/3/4):")
    result = 0
 
    if choice == '1':
        result = Calculator.add(num1, num2)
    elif choice == '2':
        result = Calculator.subtract(num1, num2)
    elif choice == '3':
        result = Calculator.multiply(num1, num2)
    elif choice == '4':
        result = Calculator.divide(num1, num2)
    else:
        print("Invalid Input")
 
    print("Result = ", result)
 
    quit = input("Do you want to continue (y/n) ?")
    if quit == 'n':
        break
